

## 鸣谢

首先感谢表哥的chemex咖啡壶。感谢大表哥

[Laravel](https://laravel.com/) ，优雅的 PHP Web 框架。

[Dcat Admin](https://dcatadmin.com) ，高颜值、高效率的后台开发框架。

。

## 简介

这款问题 追踪系统主要对公司安全 卫生 警卫 一系统工需追 的问题进行跟踪 员工录入系统 领导定时检查问题 的状态 和难点 
#
命令一步步执行，一般是不会有部署问题的。

1：为你的计算机安装 `PHP` 环境，参考：[PHP官方](https://www.php.net/downloads) 推荐 phpstudy 。

2：为你的计算机安 mysql 最新phpstudy 自带mysql最新版。

3：创建一个数据库，命名任意，但记得之后填写配置时需要对应正确，并且数据库字符集为 `utf8-general-ci`。

4：在 `.env` 中配置数据库信息以及 `APP_URL` 信息。

7：进入项目根目录，把test.sql还原到数据库中

8：你可能使用的web服务器为 `nginx` 以及 `apache`，无论怎样，应用的起始路径在 `/public` 目录，请确保指向正确。

9：修改web服务器的伪静态规则为：`try_files $uri $uri/ /index.php?$args;`。

11：为了确保在线自动更新可用，请确认网站根目录及其所有子目录的权限为可读写，即 `755`，拥有者为 `www`。

12：此时可以通过访问 `http://your_domain` 来使用管理员账号密码为：`admin / admin`。


## 截图

![](http://r.photo.store.qq.com/psc?/V10HOozO0zRf0E/TmEUgtj9EK6.7V8ajmQrEP*9HYmLw.U9yauLYow42ovsJRILc7i8FamoIhvLES2YpzmeishNR6SzzZZqDboxYCWDLUo23XUn7XgkUD7n7Ns!/r)



## 参与贡献

全程参考 chemex咖啡壶 

