<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainquestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mainquestion', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('department_id')->default('0');
            $table->integer('address_id')->nullable();
            $table->integer('result_id')->nullable();
            $table->string('question')->default('');
            $table->string('picture')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainquestion');
    }
}
