<?php 
return [
    'labels' => [
        'User' => 'User',
    ],
    'fields' => [
        'name' => '姓名',
        'email' => '邮件',
        'email_verified_at' => 'email_verified_at',
        'password' => 'password',
        'remember_token' => 'remember_token',
    ],
    'options' => [
    ],
];
