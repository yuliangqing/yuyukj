<?php
return [
    'labels' => [
        'Mainquestion' => '问题追踪表',
    ],
    'fields' => [
        'department_id' => '部门',
        'address_id' => '地点',
        'result_id' => '追踪状态',
        'question' => '问题点',
        'picture' => '图片',
    ],
    'options' => [
    ],
];
