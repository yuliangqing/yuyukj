<?php 
return [
    'labels' => [
        'Department' => 'Department',
    ],
    'fields' => [
        'name' => 'name',
        'picture' => 'picture',
    ],
    'options' => [
    ],
];
