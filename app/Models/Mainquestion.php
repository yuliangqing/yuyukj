<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class Mainquestion extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'mainquestion';

    protected $primaryKey = 'Id';



    public function department()
    {
        return $this->hasOne(Department::class,'Id','department_id');
    }

    /**
     * 制造商
     * @return HasOne
     */
    public function address()
    {
        return $this->hasOne(Address::class,'Id','address_id');
    }

    /**
     * 购入途径
     * @return HasOne
     */
    public function result()
    {
        return $this->hasOne(Result::class,'Id','result_id');
    }

    /**
     * 购入途径
     * @return HasOne
     */
}
