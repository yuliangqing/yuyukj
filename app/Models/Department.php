<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'department';

    protected $primaryKey = 'Id';
}
