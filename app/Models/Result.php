<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
	use HasDateTimeFormatter;
    protected $table = 'result';

    protected $primaryKey = 'Id';
}
