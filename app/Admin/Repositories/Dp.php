<?php

namespace App\Admin\Repositories;

use App\Models\Dp as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class Dp extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
