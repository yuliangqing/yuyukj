<?php

namespace App\Admin\Repositories;

use App\Models\Mainquestion as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class Mainquestion extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
