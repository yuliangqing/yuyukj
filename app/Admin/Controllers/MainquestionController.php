<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Mainquestion;
use App\Models\Address;
use App\Models\Department;
use App\Models\Result;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Controllers\AdminController;

class MainquestionController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Mainquestion(['department','address','result']), function (Grid $grid) {
            $grid->column('Id')->sort();
            $grid->column('department.name',admin_trans_label('部门'));
            $grid->column('address.atadress',admin_trans_label('地点'));
            //$grid->column('address_id');
            //$grid->column('result_id');
            $grid->column('question');
            $grid->column('picture')->image();
            $grid->column('result.isok',admin_trans_label('追踪状态'));
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();

            $grid->filter(function (Grid\Filter $filter) {

                $filter->like('department.name',admin_trans_label('部门'));
                $filter->like('address.atadress',admin_trans_label('地点'));
                $filter->like('question');
                $filter->like('result.isok',admin_trans_label('追踪状态'));




            });

            $grid->filter(function (Grid\Filter $filter) {
                // 更改为 panel 布局
                $filter->panel();

                // 注意切换为panel布局方式时需要重新调整表单字段的宽度
                $filter->equal('id')->width(3);
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Mainquestion(), function (Show $show) {
            $show->field('Id');
            $show->field('department_id');
            $show->field('address_id');
            $show->field('result_id');
            $show->field('question');
            $show->field('picture');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Mainquestion(), function (Form $form) {
            $form->display('Id');
            //$form->text('department_id');
            $form->select('department_id',admin_trans_label('部门'))
                ->options(Department::all()
                    ->pluck('name', 'Id'));
            //$form->text('address_id');
            $form->select('address_id',admin_trans_label('地点'))
                ->options(Address::all()
                    ->pluck('atadress', 'Id'));
            //$form->text('result_id');

            $form->text('question');
           // $form->text('picture');
            $form->image('picture')
                ->autoUpload()
                ->uniqueName();
            $form->select('result_id',admin_trans_label('追踪状态'))
                ->options(Result::all()
                    ->pluck('isok', 'Id'));
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
