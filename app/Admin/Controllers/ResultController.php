<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Result;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Controllers\AdminController;

class ResultController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Result(), function (Grid $grid) {
            $grid->column('Id')->sortable();
            $grid->column('isok');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('Id');
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Result(), function (Show $show) {
            $show->field('Id');
            $show->field('isok');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Result(), function (Form $form) {
            $form->display('Id');
            $form->text('isok');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
